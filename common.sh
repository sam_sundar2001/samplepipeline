log() {
    >&2 echo "# $1"
}

get_branch_version() {
    # branch name should look something linke this: release/x.y.z
    # x, y and z should be numbers
    [[ $BITBUCKET_BRANCH =~ ^release/([0-9]+\.[0-9]+\.[0-9]+)$ ]] &&
        echo "${BASH_REMATCH[1]}"
}

get_phase() {
    [[ $BITBUCKET_BRANCH == "master" ]] && echo alpha.
    return 0
}

get_version() {
    if [[ $BITBUCKET_BRANCH == dev ]]; then
        get_source_version
    else
        v=$(get_branch_version)

        if [[ -z $v ]]; then
            log "invalid release branch name: $BITBUCKET_BRANCH"
            return 1
        else
            echo $v
        fi
    fi
}

build_docker_image() {
    local app=$1
    local phase=$2
    local ver=$3

    tag=$ver-$phase$BITBUCKET_BUILD_NUMBER.r${BITBUCKET_COMMIT:0:7}
    echo "new tag: $tag"

    name=$DOCKER_SERVER/$app:$tag
    echo "new image: $name"

    echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_LOGIN" --password-stdin "$DOCKER_SERVER"
    docker build --no-cache -t "$name" .
    docker push "$name"    
}