get_branch_version() {
    # branch name should look something linke this: release/x.y.z
    # x, y and z should be numbers
  if [[ $BITBUCKET_BRANCH =~ ^release/([0-9]+\.[0-9]+\.[0-9]+)$ ]]
  then
      echo "${BASH_REMATCH[1]}"
      A="release"
  elif [[ $BITBUCKET_BRANCH =~ ^hotfix/([0-9]+\.[0-9]+\.[0-9]+)$ ]]
  then
     echo "${BASH_REMATCH[1]}"
     local B="hotfix"
  fi
}
get_phase() {
if [[ $BITBUCKET_BRANCH =~ ^release/([0-9]+\.[0-9]+\.[0-9]+)$ ]]
  then
    A="release"
   echo $A
elif [[ $BITBUCKET_BRANCH =~ ^hotfix/([0-9]+\.[0-9]+\.[0-9]+)$ ]]
  then
    B="hotfix"
    echo $B
elif [ $BITBUCKET_BRANCH == dev ]
   then
     echo "alpha"
fi
}
PHASE=$(get_phase)
echo $PHASE
BRANCH=$(get_branch_version)
echo $BRANCH
TAG=$DOCKER_SERVER/$DOCKER_USERNAME/$BRANCH-$PHASE.$BITBUCKET_BUILD_NUMBER.r${BITBUCKET_COMMIT:0:7}
docker build -t $TAG .
docker push $TAG
echo $TAG